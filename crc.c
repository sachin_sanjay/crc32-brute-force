// sources:https://rosettacode.org/wiki/CRC-32 http://www.reversing.be/article.php?story=20061209172739930 https://github.com/kmyk/zip-crc-cracker
//program is made to handel only 4 bytes
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
static uint32_t table[256];
static int have_table = 0;
uint32_t test;
void rc_crc32()
{
	uint32_t rem;
	
	int i, j;
	if (have_table == 0) {
		for (i = 0; i < 256; i++) {
			rem = i;  /* remainder from polynomial division */
			for (j = 0; j < 8; j++) {
				if (rem & 1) {
					rem >>= 1;
					rem ^= 0xedb88320;
				} else
					rem >>= 1;
			}
			table[i] = rem;
			// printf("%x--->%x\n",i,rem); //use this to print the full table 
		}
		have_table = 1;
	}

}
uint32_t crc_maker(uint32_t crc, const char *buf, size_t len){
	const char *p, *q;
	crc = ~crc;
	uint8_t octet;
	q = buf + len;
	for (p = buf; p < q; p++) {
		octet = *p;  /* Cast to unsigned octet. */
		// printf("oct-> %c crc-> %x xor-> %x table->%x\n",octet,crc,(crc & 0xff),((crc & 0xff) ^ octet));
		crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
	}
	return ~crc;
}
int word_generator(char *s){
	int bool=0;
	// below are the most probable alphabets if u want to add more plz increase to for loop ranges also
	char alphabet[]={ 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 58, 59, 60, 61, 62, 63, 64, 91, 92, 93, 94, 95, 96, 123, 124, 125, 126, 32, 9, 10, 13, 11, 12 };
	for (int i=0;i<99;i++){
		for(int j=0;j<99;j++){
			for(int k=0;k<99;k++){
				for(int l=0;l<99;l++){
					s[0]=alphabet[i];s[1]=alphabet[j];s[2]=alphabet[k];s[3]=alphabet[l];
					if(crc_maker(0, s, strlen(s))==test){
						return(1);
					}
				}
			}
		}
	}
	return(0);
}
int main(int argc,char *argv[])
{
	rc_crc32();
	char s[5]={'1','1','1','1','\0'}; 	
	if(argc<2)
		printf("enter crc32 as arguments\n");
	// test=0x31099e07;
	else{
		for(int i=1;i<argc;i++){
			test=(int)strtol(argv[i], NULL, 16);
			printf("%x-> ",test);
			if(word_generator(s))
 				printf("found possible match \"%s\"\n",s);
		}
 	}
	return 0;
}
